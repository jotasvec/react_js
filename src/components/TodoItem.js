import React, { Component } from 'react';
import PropTypes from 'prop-types';

class TodoItem extends Component {

    itemStyle = () => {
        return {
            backgroundColor: '#f4f4f4',
            color: '#282c34',
            padding: '10px',
            borderBottom: '1px #ccc dotted',
            margin: '5px',


            textDecoration: this.props.todo.completed ? 'line-through'  : 'none'
        }
    }

    // markComplete = () => {
    //     console.log(this.props);
    // }


  render() {
    const { id, title } = this.props.todo
    return (
      <div style={this.itemStyle()}>

        <p> 
            <input 
                type="checkbox" 
                onChange={this.props.markComplete.bind(
                    this, 
                    id
                    )} 
                style={{marginRight: '10px'}} 
            /> 
            {title}

            {/* TODO: ver como hacer desaparecer la tarea de la lista de Todos */}

            <button 
                style={btnStyle} 
                onClick={this.props.delTask.bind(this, id)}
            >x</button>
        </p>
      </div>
    )
  }
}

const btnStyle = {
    backgroundColor: 'red',
    borderRadius: '50%',
    padding: '5px 9px',
    color: '#FFF',
    cursor: 'pointer',
    float: 'right'
}

//ProtoTypes
TodoItem.propTypes = {
    todo: PropTypes.object.isRequired,
    markComplete: PropTypes.func.isRequired,
    delTask: PropTypes.func.isRequired
}

export default TodoItem;
