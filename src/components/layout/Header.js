import React from 'react';
import { Link } from 'react-router-dom';




function Header(){
    return (
        <header style={headerStyle.header}>
            <h1>TODO List</h1>
            <div className="nav-component">
                <Link to="/" className="nav-link" > Home </Link> |
                <Link to="/about" className="nav-link" > About </Link>
            </div>
        </header>
    )
}
const headerStyle = {
    header:{
        backgroundColor: '#282c34',
        color: 'white',
        alignContent: 'center',
        padding: '10px',
    },
    links:{
        color: '#fff',
        textDecoration: 'none',
        cursor: 'pointer',   
    }
}
export default Header;