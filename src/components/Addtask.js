import React, { Component } from 'react'
import PropTypes from 'prop-types'


class AddTask extends Component {
    state={
        title:''
    }
    onSubmit = (e) => {
        e.preventDefault();
        this.props.addTask(this.state.title);
        this.setState({title:''});
    }

    onChange = (e) => this.setState({
        [e.target.name]:e.target.value     
    })

    

    render() {
        return (
            <form style={AddTaskStyle.formStyle} onSubmit={this.onSubmit} >
                <input
                    type="text"
                    name="title"
                    placeholder="Add task todo...."
                    style={AddTaskStyle.txtInputStyle}
                    value={this.state.title}
                    onChange={this.onChange}
                />
                <input
                    type="submit"
                    value="Add"
                    className="btn"
                    style={AddTaskStyle.btnStyle}
                />
            </form>
        )
  }
}

const AddTaskStyle = {
    formStyle:{
        display: 'flex',
        
    },
    txtInputStyle:{
        flex: '10',
        padding: '5px',
    },
    btnStyle:{
        flex: '1'
    }
}

//ProtoTypes
AddTask.propTypes = {
    addTask: PropTypes.func.isRequired
}

export default AddTask;