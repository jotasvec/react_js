import React, { Component } from 'react';

export default class ShowTodos extends Component {
  render() {
    return (
        <div className="show-list" style={showListStyles.showList}> 
            <h4 style={showListStyles.title}>Show list</h4>
            <div className="show-list-btns" style={showListStyles.btns}>
                <button className="btn showlist-btn">All</button>
                <button className="btn showlist-btn">Todo</button>
                <button className="btn showlist-btn">Done</button>
            </div>
        </div>
    );
  }
}

const showListStyles = {
    showList:{
        display: 'flex',
        flexDirection: 'row',
    },
    title:{
        flex: '2',
        alignSelf: 'center',
        margin: '0px',
        color: '#282c34',
        
        
    },
    btns:{
        flex:'9',
        margin: '0',
        
    }

}