import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom'; //generar rutas en js
import Todos from './components/Todos';
import ShowTodos from './components/ShowTodos'
import Header from './components/layout/Header'
import AddTask from './components/Addtask';
import About from './components/pages/About';

// import uuid from 'uuid'; //uuid permite generar id's aleatoreos

// a modo de prueba se usa jsonplaceholder, y se haran los request con axion
import axios from 'axios';

// import logo from './logo.svg';
import './App.css';

class App extends Component {
  state ={
    todos:[]
    //   {
    //     id: uuid.v4(),
    //     title: 'Finish the todo app',
    //     done: false
    //   },
    //   {
    //     id: uuid.v4(),
    //     title: 'Finish the todo app 2',
    //     done: false
    //   },
    //   {
    //     id: uuid.v4(),
    //     title: 'Finish the todo app 3',
    //     done: false
    //   }
    // ]
  }

  // Didmount -> se agrega el didmount para usar axios y alimentar el state
  componentDidMount(){
    axios.get('https://jsonplaceholder.typicode.com/todos?_limit=10')
    .then(
      res => this.setState({todos: res.data})
    )
  }

  //Toggle completed task
  markComplete = (id) => {
    this.setState({ todos: this.state.todos.map( todo => {
      if(todo.id === id){
        todo.completed = !todo.completed
      }
      return todo;
    })
  });
  }

  // Delete Task
  delTask = (id) => {
    // this.setState({
    //   todos: [...this.state.todos.filter(
    //     todo => todo.id !== id
    //   )]
    // });

    //eliminar la tareea de la lista usando axios 
    axios.delete(`https://jsonplaceholder.typicode.com/todos/${id}`)
    .then(res => this.setState({
        todos: [...this.state.todos.filter(
          todo => todo.id !== id
        ), res.data]
      })
    )
  }

  addTask = (title) =>{
    // const newTask = {
    //     id: uuid.v4(),
    //     title,
    //     done: false
    // }
    // this.setState({
    //   todos: [...this.state.todos, newTask ]
    // })

    // con axios seria asi
    axios.post('https://jsonplaceholder.typicode.com/todos', {
      title,
      completed: false
    })
    .then( res =>
      this.setState({
        todos: [...this.state.todos, res.data ]
      })
    );
  }

  render(){
    return (
      <Router>
        <div className="App">
          <Header/>   
          <Route exact path="/" render={props => (
            <React.Fragment>
              <AddTask 
                addTask={this.addTask}
              />    
              <ShowTodos/> 
              <Todos 
                todos={this.state.todos} 
                markComplete={this.markComplete}
                delTask={this.delTask}
              />    
            </React.Fragment>
          )} /> {/* end classname app */}
          <Route path="/about" component={About} />
      </div>
    </Router>
    );
}
}

export default App;
